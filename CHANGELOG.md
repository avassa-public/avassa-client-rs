# Changelog

All notable changes to this project will be documented in this file.

## [0.6.0] - 2025-02-11

### 🚀 Features

- New fields in supd and application metrics
- Added position since for consumers
- Moved client to feature

### 🐛 Bug Fixes

- Removed unused dep
- Updated lockfile

### ⚙️ Miscellaneous Tasks

- Upped dependencies

## [0.2.11] - 2023-01-31

### ⚙️ Miscellaneous Tasks

- Release avassa-client version 0.2.11

## [0.2.10] - 2022-11-08

### ⚙️ Miscellaneous Tasks

- Release avassa-client version 0.2.10

## [0.2.9] - 2022-11-07

### ⚙️ Miscellaneous Tasks

- Release avassa-client version 0.2.9

## [0.2.8] - 2022-11-07

### ⚙️ Miscellaneous Tasks

- Release avassa-client version 0.2.7
- Release avassa-client version 0.2.8

## [0.2.6] - 2022-11-04

### ⚙️ Miscellaneous Tasks

- Release avassa-client version 0.2.6


