/// Application/container metrics
#[derive(Debug, Clone, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
#[non_exhaustive]
pub struct MetricEntry {
    /// Metric timestamp
    pub time: chrono::DateTime<chrono::Utc>,
    /// Site name
    pub site: String,
    /// Cluster host
    pub cluster_hostname: String,
    /// Site host
    pub hostname: String,

    /// CPU metrics
    pub cpu: CPU,

    /// Memory metrics
    pub memory: Memory,
}

#[derive(Debug, Clone, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
#[non_exhaustive]
pub struct CPU {
    pub nanoseconds: u64,
    // CPU limit for the supd container
    pub cpus: f64,
    // percentage used in relation to limits
    pub percentage_used: f64,
}

#[derive(Debug, Clone, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
#[non_exhaustive]
pub struct Memory {
    pub used: u64,
    pub total: u64,
    pub percentage_used: f32,
    pub used_hot: u64,
    pub percentage_used_hot: f32,
}
